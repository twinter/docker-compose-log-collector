# Log analytics with Kibana and Grafana based on Elasticsearch

A log collector/aggregator based on logstash, logspout and metricbeat

## Download

1. clone the repository to a folder (\~/docker/kibana-grafana in the example) with `git clone https://gitlab.com/twinter/docker-compose-log-collector ~/docker/log-collector`



## Setup

1. if you want the beats to set up premade dashboards:
    - change the last line in metricbeat.yml to `  dashboards.enabled: true`
    - change the last line in filebeat.yml to `  dashboards.enabled: true`

2. create a new role in elasticsearch named "monitoring_ingress" (you can change the name but remember to also change it in all following steps) with the following settings:
    - cluster privileges:
        - monitor_watcher
        - cluster:monitor/xpack/license/get
        - manage_index_templates
        - manage_logstash_pipelines
    - index privileges for indices "logstash-\*", "metricbeat-\*", "filebeat-\*",  ".logstash" and "logstash":
        - create
        - monitor
        - write
        - manage
        - view_index_metadata
        - maintenance

3. create a new user for your monitored machine with the following roles:
    - monitoring_ingress (this is the one from the previous step)
    - beats_system
    - remote_monitoring_agent
    - logstash_system
    - kibana_admin (only if you want the beats to create premade dashboards, see step 1)
    
4. create your own elastic_credentials.env and .env files based on the provided template files (remember to use a strong password for elasticsearch if you plan on exposing it to the outside). It is advised to create a new user for metricbeat, the current default is the root user of the elasticsearch database, see below for instructions on that.

5. (optional) add some labels to you docker containers to be able to easily identify them
### TODO: add labels/names to other containers


## Startup

### With the [docker-compose systemd service](https://gitlab.com/twinter/docker-compose-systemd-service) (recommended as a permanent setup)

1. setup the docker-compose systemd service as described here: https://gitlab.com/twinter/docker-compose-systemd-service

2. create a symlink from the projects folder to /etc/docker/compose with `sudo ln -s /home/your_username/docker/log-collector /etc/docker/compose/log-collector`

3. start and enable the service with `sudo systemctl enable --now docker-compose@log-collector`

### With only docker-compose (recommended only for testing)

Run `sudo docker-compose up` in the projects folder on your system.



## Usage

### Set the log format for containers

Add the com.github.gliderlabs.logspout.format label to your docker containers. Have a look at ./logstash-pipelines/ to see what log formats are supported by pipeline filters.
It should look something like this in your docker-compose file:
```
    labels:
      com.github.gliderlabs.logspout.format: "nginx"
```

If you don't find a fitting style add your own format specific pipeline filter to logstash if you're missing a format (and create a pull request to share your filter.
Predefined filter patterns can be found here: https://github.com/logstash-plugins/logstash-patterns-core/tree/master/patterns
Try to make them fitting to the others (same name for fields with the same meaning).



## Troubleshooting
