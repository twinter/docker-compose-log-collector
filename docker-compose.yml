version: '3.4'

x-metricbeat-defaults: &metricbeat_defaults
  # no :latest option available, check for newer versions from time to time
  image: docker.elastic.co/beats/metricbeat:7.10.1
  user: root
  hostname: ${HOSTNAME}
  command: --strict.perms=false
  restart: unless-stopped
  env_file:
    - elastic_credentials.env
  environment:
    - -system.hostfs=/hostfs

services:
  logstash:
    # no :latest option available, check for newer versions from time to time
    # details: https://www.elastic.co/guide/en/logstash/current/docker-config.html
    # keep in mind that the logstash version has to be the same as your elasticsearch version
    image: docker.elastic.co/logstash/logstash:7.10.1
    restart: unless-stopped
    volumes:
      - ./logstash.yml:/usr/share/logstash/config/logstash.yml
      - ./logstash/patterns/:/opt/logstash/patterns/
      - ./logstash/pipelines/:/usr/share/logstash/pipeline/
    env_file:
      - elastic_credentials.env

  metricbeat:
    # we need one instance inside the docker-compose default network to monitor logstash
    << : *metricbeat_defaults
    volumes:
      - ./metricbeat.yml:/usr/share/metricbeat/metricbeat.yml:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - /sys/fs/cgroup:/hostfs/sys/fs/cgroup:ro
      - /proc:/hostfs/proc:ro
      - /:/hostfs:ro

  metricbeat-external:
    # we need another instance connected directly to the host to monitor network traffic accurately
    << : *metricbeat_defaults
    network_mode: host
    volumes:
      - ./metricbeat-external.yml:/usr/share/metricbeat/metricbeat.yml:ro
      - /proc:/hostfs/proc:ro
      - /sys/fs/cgroup:/hostfs/sys/fs/cgroup:ro
      - /:/hostfs:ro

  filebeat:
    image: docker.elastic.co/beats/filebeat:7.10.1
    user: root
    command: --v --e --strict.perms=false
    hostname: ${HOSTNAME}
    restart: unless-stopped
    volumes:
      - ./filebeat.yml:/usr/share/filebeat/filebeat.yml:ro
      - /var/lib/docker/containers:/var/lib/docker/containers:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
    env_file:
      - elastic_credentials.env

  # TODO: configure firewall
  # TODO: reduce code duplication in metricbeat config with https://www.elastic.co/guide/en/beats/metricbeat/current/metricbeat-configuration-reloading.html
  # TODO: add filebeat? (for sshd and eventually elasticsearch)
  # TODO: add something to get syslogs
  # TODO: add healtchecks for containers, for examples see https://github.com/elastic/stack-docker
